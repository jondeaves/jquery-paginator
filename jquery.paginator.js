(function ($) {

    $.fn.paginator = function (options) {

        var context = $(this),
            context_body = context.children('tbody'),
            context_footer = context.children('tfoot');

        var defaults = {
            pageSplit   : 20,
            startPage   : 1,
            ajax        : false,
            ajaxURL     : '',
            ajaxType    : 'GET',
            source      : null,

            first       : null,
            last        : null,
            previous    : null,
            next        : null,
            change      : null

        };
        var options = $.extend({}, defaults, options);


        // Verify data
        var total_results = context.children('tbody').children('tr').length,
            total_page = Math.ceil(total_results / options.pageSplit);

        if(options.startPage > total_page) { options.startPage = total_page; }
        if(options.startPage <= 0) { options.startPage = 1; }
        if(total_page < 1) { total_page = 1; }
        var current_page = options.startPage;




        // Mark-up each block of results for each page
        var add_source_rows = function(body, data)
        {

            for(var i_row = 0; i_row < data.length; i_row++)
            {

                var row = data[i_row];
                var new_row = $('<tr></tr>');

                for(var i_col = 0; i_col < row.length; i_col++)
                {
                    new_row.append($('<td>'+row[i_col]+'</td>'))
                }

                body.append(new_row);

            }

        };

        var add_row_class = function()
        {

            var body_count = context_body.children('tr').length;
            total_results = context.children('tbody').children('tr').length;
            total_page = Math.ceil(total_results / options.pageSplit);

            if (current_page > total_page) {
                current_page = total_page;
            }
            $('.jquery-paginator .paginator_page_number').val(current_page);


            for (var i_result = 0; i_result < body_count; i_result++) {

                var page_number = Math.ceil((i_result + 1) / options.pageSplit),
                    this_row = context_body.children('tr').eq(i_result);

                this_row.removeClass();

                if (page_number == 0) {
                    page_number = 1;
                }


                this_row.addClass('paginator_page_count_' + page_number);
                this_row.attr('data-page', page_number);
                if (page_number != current_page) {
                    this_row.hide();
                } else {
                    this_row.css('display', '');
                }

                this_row.children('td').eq(1).children('i.icon-remove-blacklist').attr('id-index', i_result);
            } // close for(var i_result = 0; i_result < context_body.length; i_result++)

            toggle_buttons();

        } // close var add_row_class = function() {


        // Enable/Disable buttons that can/cannot be used
        var toggle_buttons = function()
        {

            if(current_page > 1)
            {
                // Can move backwards
                $('.paginator .paginator_first_btn').removeAttr('disabled');
                $('.paginator .paginator_prev_btn').removeAttr('disabled');

            } // close if(current_page > 1)
            else
            {
                $('.paginator .paginator_first_btn').attr('disabled', 'true');
                $('.paginator .paginator_prev_btn').attr('disabled', 'true');
            } // close if(current_page > 1) [else]


            if(current_page < total_page)
            {
                // Can move forwards
                $('.paginator .paginator_next_btn').removeAttr('disabled');
                $('.paginator .paginator_last_btn').removeAttr('disabled');
            } // close if(current_page < total_page)
            else
            {
                $('.paginator .paginator_next_btn').attr('disabled', 'true');
                $('.paginator .paginator_last_btn').attr('disabled', 'true');
            } // close if(current_page < total_page) [else]

        } // close var toggle_buttons = function()


        /*
         * Build paginator footer, numbers and input box
         */
        var build_paginator = function(start_page)
        {

            var paginator_output = '';
            paginator_output += '<div class="input-group jquery-paginator">';
            paginator_output += '<span class="input-group-btn paginator">';
            paginator_output += '<button type="button" class="btn btn-default paginator_first_btn" title="First Page"><i class="fa fa-angle-double-left"></i></button>';
            paginator_output += '<button type="button" class="btn btn-default paginator_prev_btn" title="Previous Page"><i class="fa fa-angle-left"></i></button>';
            paginator_output += '</span>';

            paginator_output += '<input type="text" class="form-control paginator_page_number numeric" value="'+options.startPage+'" style="text-align:center;">';

            paginator_output += '<span class="input-group-btn paginator">';
            paginator_output += '<button type="button" class="btn btn-small btn-default paginator_next_btn" title="Next Page"><i class="fa fa-angle-right"></i></button>';
            paginator_output += '<button type="button" class="btn btn-small btn-default paginator_last_btn" title="Last Page"><i class="fa fa-angle-double-right"></i></button>';
            paginator_output += '<button type="button" class="btn btn-small btn-default btn-info paginator_go_number">Go</button>';
            paginator_output += '</span>';

            paginator_output += '</div>';

            return paginator_output;

        }


        var build_output = function()
        {


            /*
             * If ajax or source, replace tbody with contents of source/ajax
             */
            if(options.source != null && jQuery.isArray(options.source) && options.source.length > 0)
            {
                context_body.html('');
                context_body.append(add_source_rows(context_body, options.source));
                add_row_class();
            }
            else if(options.ajax === true)
            {

                var data = '';
                $.ajax({
                    url : options.ajaxURL,
                    type : options.ajaxType.toUpperCase(),
                    success : function(response){
                        context_body.html('');
                        context_body.append(add_source_rows(context_body, JSON.parse(response)));
                        add_row_class();
                    }

                });

                context_body.append(add_source_rows(context_body, data));
            }
            else
            {
                add_row_class();
            }



            /*
             * Add paginator buttons to footer of table if total results require more than one page
             */
            if(total_results > options.pageSplit)
            {
                var result = build_paginator(options.startPage);
                if(context_footer.length <= 0)
                    context_footer = $('<tfoot><tr><td colspan="2">'+result+'</td></tr></tfoot>');
                else
                    context_footer.children('tr').children('td').eq(0).html(result);

                context.append(context_footer);
            } // close if(total_results > options.pageSplit)



            /*
             * When manually typing page number, highlight all text
             */
            $('body').delegate('.jquery-paginator .paginator .paginator_page_number', 'focus', function(){ $(this).select(); });
            // Bind to navigation change
            $('.paginator .paginator_number').click(function(){
                var page_number = $(this).attr('id').replace('paginator_page_', '');
                context_body.children('tr').hide();
                context_body.children('tr.paginator_page_count_' + page_number).show();
            }); // close $('.paginator .paginator_number').click(function(){

            $('.paginator .paginator_first_btn').click(function(){ context.trigger('firstPage'); });
            $('.paginator .paginator_prev_btn').click(function(){ context.trigger('previousPage'); });
            $('.paginator .paginator_next_btn').click(function(){ context.trigger('nextPage'); });
            $('.paginator .paginator_last_btn').click(function(){ context.trigger('lastPage'); });
            $('.paginator .paginator_go_number').click(function(){ context.trigger('changePage', $('.jquery-paginator .paginator_page_number').val() ) });


            // Trigger page changes based on interaction
            var next_page = 1;
            context.bind("firstPage", function(event, data){
                context.trigger("changePage", 1);
                var callbackOptions = { 'previous_page' : current_page, 'current_page' : next_page };
                if(options.first !== undefined && options.first !== null){ options.first(callbackOptions); }
            });

            context.bind("previousPage", function(event, data){
                context.trigger("changePage", current_page - 1);
                var callbackOptions = { 'previous_page' : current_page, 'current_page' : next_page };
                if(options.previous !== undefined && options.previous !== null){ options.previous(callbackOptions); }
            });

            context.bind("nextPage", function(event, data){
                context.trigger("changePage", current_page + 1);
                var callbackOptions = { 'previous_page' : current_page, 'current_page' : next_page };
                if(options.next !== undefined && options.next !== null){ options.next(callbackOptions); }
            });

            context.bind("lastPage", function(event, data){
                context.trigger("changePage", total_page);
                var callbackOptions = { 'previous_page' : current_page, 'current_page' : next_page };
                if(options.last !== undefined && options.last !== null){ options.last(callbackOptions); }
            });

            context.bind("changePage", function(event, data){

                if( Math.floor(data) == data && $.isNumeric(data))
                {
                    if(data > total_page) { data = total_page; }
                    if(data <= 0) { data = 1; }
                    current_page = data;
                    $('.jquery-paginator .paginator_page_number').val(data);
                    context_body.children('tr').hide();
                    context_body.children('tr.paginator_page_count_' + data).show();

                    var callbackOptions = { 'previous_page' : current_page, 'current_page' : data };
                    if(options.change !== undefined && options.change !== null) { options.change(callbackOptions); }

                    next_page = data;

                } // close if( Math.floor(data) == data && $.isNumeric(data))

                toggle_buttons();

            }); // Takes page number as input

            // Add item to main tr
            context.bind("addItem", function(event, data){

                var body_count = context_body.children('tr').length,
                    last_count = 0,
                    newEle = $(data);

                if(total_page < 1) { total_page = 1; }

                for(var i_result = 0; i_result < body_count; i_result++)
                {
                    var this_row = context_body.children('tr').eq(i_result);
                    if( this_row.hasClass('paginator_page_count_' + total_page) ) {
                        last_count++;
                    }
                } // close for(var i_result = 0; i_result < body_count; i_result++)




                if(last_count < options.pageSplit)
                {
                    // Append to last page
                    newEle.addClass('paginator_page_count_'+total_page);
                    if(current_page != total_page) { newEle.css('display','none'); }
                    context_body.append(newEle);
                } // close if(last_count < options.pageSplit)
                else
                {
                    // Need to create new page
                    newEle.addClass('paginator_page_count_'+(total_page + 1)+'');
                    newEle.css('display','none');
                    context_body.append(newEle);

                    total_results++;
                    total_page = Math.ceil(total_results / options.pageSplit);
                } // close if(last_count < options.pageSplit)

                add_row_class();

            }); // Take <tr> block as input

            // Remove item from main tr
            context.bind("removeItem", function(event, data) {
                if( Math.floor(data) == data && $.isNumeric(data))
                {
                    context_body.children('tr').eq(data).remove();
                    add_row_class();

                } // close if( Math.floor(data) == data && $.isNumeric(data))
            }); // Takes row number as input

        }


        /*
         * Initialisation
         */
        build_output();

    }

})(jQuery);
